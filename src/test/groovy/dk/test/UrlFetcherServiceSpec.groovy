package dk.test

import groovyx.net.http.RESTClient
import spock.lang.Specification

class UrlFetcherServiceSpec extends Specification {
    UrlFetcherService sut

    def setup() {
        sut = new UrlFetcherService()
        sut.restClient = Mock(RESTClient)
    }

    def "test that UrlFetcherService will return a list of jokes if the service responds"() {
        setup:
        def response = [
                status: 200,
                responseData: [[value: [id: 1, joke: "Test", categories: ["nerdy"]]]]
        ]

        when:
        def jokes = sut.jokes

        then:
        1 * sut.restClient.get(_) >> response

        and:
        jokes.size() == 1
        jokes.first().id == 1
        jokes.first().joke == "Test"
        jokes.first().categories == ["nerdy"]
    }

    def "test that UrlFetcherService will return an empty list of jokes if the service does not responds"() {
        setup:
        def response = [
                status: 400
        ]

        when:
        def jokes = sut.jokes

        then:
        1 * sut.restClient.get(_) >> response

        and:
        !jokes
    }

    def "test that UrlFetcherService will return an empty list of jokes if the service throws an exception"() {
        when:
        def jokes = sut.jokes

        then:
        1 * sut.restClient.get(_) >> { throw new IOException("Service error")}

        and:
        !jokes
    }
}
