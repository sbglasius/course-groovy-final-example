package dk.test

import groovy.transform.InheritConstructors
import spock.lang.Specification
import spock.lang.Unroll


class FileStorageServiceSpec extends Specification {
    FileStorageService sut

    def setup() {
        sut = new FileStorageService(file: Mock(FileWrapper))
    }

    void "if file read fails an empty list of jokes will be returned"() {
        given:
        sut.file.getText() >> { throw new IOException("Error")}

        expect:
        sut.jokes == []
    }

    @Unroll
    void "a valid xml will output the correct list of jokes"() {
        given:
        sut.file.getText() >> xml

        expect:
        sut.jokes == expectedJokes

        where:
        expectedJokes                                     | xml
        []                                                | "<jokes></jokes>"
        [new Joke(1, "J1", [])]                           | "<jokes><joke id='1'><text>J1</text></joke></jokes>"
        [new Joke(1, "J1", ["A"])]                        | "<jokes><joke id='1'><text>J1</text><category>A</category></joke></jokes>"
        [new Joke(1, "J1", ["A", "B"])]                   | "<jokes><joke id='1'><text>J1</text><category>A</category><category>B</category></joke></jokes>"
        [new Joke(1, "J1", ["A"]), new Joke(1, "J2", [])] | "<jokes><joke id='1'><text>J1</text><category>A</category></joke><joke id='1'><text>J2</text></joke></jokes>"
    }

    @Unroll
    def "a list of jokes will be stored as a correct xml document"() {
        when:
        sut.jokes = jokes

        then:
        1 * sut.file.leftShift({ it.toString() == expectedXml })

        where:
        jokes                                             | expectedXml
        []                                                | "<jokes></jokes>"
        [new Joke(1, "J1", [])]                           | "<jokes><joke id='1'><text>J1</text></joke></jokes>"
        [new Joke(1, "J1", ["A"])]                        | "<jokes><joke id='1'><text>J1</text><category>A</category></joke></jokes>"
        [new Joke(1, "J1", ["A", "B"])]                   | "<jokes><joke id='1'><text>J1</text><category>A</category><category>B</category></joke></jokes>"
        [new Joke(1, "J1", ["A"]), new Joke(1, "J2", [])] | "<jokes><joke id='1'><text>J1</text><category>A</category></joke><joke id='1'><text>J2</text></joke></jokes>"
    }
}

@InheritConstructors
class FileWrapper extends File {
    def leftShift(def text) {
        metaClass.leftShift(text)
    }

    def getText() {
        metaClass.getText()
    }
}
