package dk.test

import groovyx.net.http.RESTClient


class UrlFetcherService {
    private static ICNDB_URL = "http://api.icndb.com/"

    private RESTClient restClient

    UrlFetcherService() {
        restClient = new RESTClient(ICNDB_URL)
    }

    List<Joke> getJokes() {
        try {
            def response = restClient.get(path: '/jokes')
            if (response.status == 200) {
                return response.responseData.value.collect {
                    new Joke(it)
                }
            }
        } catch (ignore) {}
        return []

    }
}
