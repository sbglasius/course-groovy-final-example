package dk.test

def file = new File('./jokes.xml')
println file.absolutePath
if(!file.exists()) {
    file.createNewFile()
}
def urlFetcherService = new UrlFetcherService()
def fileStorageService = new FileStorageService(file: file)
def List<Joke> jokes = fileStorageService.jokes
if(!jokes) {
    jokes = urlFetcherService.jokes
    fileStorageService.jokes = jokes
}
def htmlService = new HtmlService(output: System.out)


