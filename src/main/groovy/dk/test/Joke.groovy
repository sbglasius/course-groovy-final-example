package dk.test

import groovy.transform.Immutable

@Immutable
class Joke {
    Integer id
    String joke
    List<String> categories
}
