package dk.test

import groovy.xml.StreamingMarkupBuilder


class FileStorageService {
    File file

    List<Joke> getJokes() {
        try {
            def jokesText = file.text
            def jokesXml = new XmlSlurper().parseText(jokesText)

            return jokesXml.joke.collect {
                def id = it.@id.toInteger()
                def joke = it.text.toString()
                def categories = it.category*.toString()
                new Joke(id: id, joke: joke, categories: categories)
            }
        } catch (ignore) {
            return []
        }
    }

    void setJokes(List<Joke> jokesList) {
        def builder = new StreamingMarkupBuilder()

        file << builder.bind {
            delegate.jokes {
                jokesList.each { j ->
                    joke(id: j.id) {
                        text(j.joke)
                        j.categories.each { c ->
                            category(c)
                        }
                    }
                }
            }
        }

    }
}
